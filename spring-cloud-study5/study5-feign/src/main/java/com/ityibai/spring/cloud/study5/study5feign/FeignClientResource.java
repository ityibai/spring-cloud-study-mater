/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 */
package com.ityibai.spring.cloud.study5.study5feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by panda on 2017/7/20.
 * Version : 1.0
 * Description : com.ityibai.spring.cloud.study5.study5feign.FeignClientResource
 */
@RestController
public class FeignClientResource {
    @Autowired
    private ComputeClient computeClient;
    @RequestMapping("/add")
    public Object test() {
        return computeClient.add(1, 2);
    }
}
