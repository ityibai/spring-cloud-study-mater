package com.ityibai.spring.cloud.study5.study5eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Study5EurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Study5EurekaApplication.class, args);
	}
}
