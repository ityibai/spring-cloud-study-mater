package com.ityibai.spring.cloud.study3.serviceb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Study3ServiceBApplication {

	public static void main(String[] args) {
		SpringApplication.run(Study3ServiceBApplication.class, args);
	}
}
