/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 */
package com.ityibai.spring.cloud.study2;

import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * Created by panda on 2017/7/18.
 * Version : 1.0
 * Description : com.ityibai.spring.cloud.study2.IComputeClient
 */
@FeignClient("compute-service")
public interface IComputeClient extends ComputeClient {
}
