/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 */
package com.ityibai.spring.cloud.study2;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * Created by panda on 2017/7/12.
 * Version : 1.0
 * Description : com.ityibai.spring.cloud.study2.FeignProcessor
 */
public class FeignProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        if (o instanceof FactoryBean) {
            Class clazz = ((FactoryBean) o).getObjectType();
            if (clazz.getAnnotation(FeignClient.class) != null) {
                System.out.println(clazz.getName());
            }
        }
        return o;
    }
}
