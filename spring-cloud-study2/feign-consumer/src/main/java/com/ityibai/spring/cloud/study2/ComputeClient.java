/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 */
package com.ityibai.spring.cloud.study2;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by panda on 2017/6/8.
 * Version : 1.0
 * Description : com.ityiba.spring.cloud.study2.ComputeClient
 */

//@RequestMapping
public interface ComputeClient {
    @RequestMapping(method = RequestMethod.GET, value = "/add")
    Integer add(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b);

    @RequestMapping(method = RequestMethod.GET, value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE)
    User user(@RequestBody User user);

    @RequestMapping(value = "/user",
            method = RequestMethod.POST,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    User create(@RequestBody User user);

    @RequestMapping(value = "/user/{id}",
            method = RequestMethod.PUT,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    User update(@PathVariable("id") Integer id, @RequestBody User user);

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    User delete(@PathVariable("id") Integer id);

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    User find(@PathVariable("id") Integer id);

    @RequestMapping(value = "/users", method = RequestMethod.GET,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    List<User> list();

}
