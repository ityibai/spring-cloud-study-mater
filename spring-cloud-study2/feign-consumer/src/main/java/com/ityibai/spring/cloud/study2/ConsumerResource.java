/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 */
package com.ityibai.spring.cloud.study2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by panda on 2017/6/8.
 * Version : 1.0
 * Description : com.ityiba.spring.cloud.study2.ConsumerResource
 */
@RestController
@RequestMapping(value = "client")
public class ConsumerResource {
    @Autowired
    IComputeClient computeClient;
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public Integer add() {
        return computeClient.add(10, 20);
    }

    @RequestMapping(value = "/user",
            method = RequestMethod.GET,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public User user() {
        User user = new User();
        user.setAge(19);
        user.setName("keyo");
       // return computeClient.user("keyo", 18);
        return computeClient.create(user);
    }

    @RequestMapping(value = "/user",
            method = RequestMethod.POST,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public User create(@RequestBody User user) {
        return computeClient.create(user);
    }

    @RequestMapping(value = "/user/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public User update(@PathVariable("id") Integer id, @RequestBody User user) {
        return computeClient.update(id, user);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public User delete(@PathVariable("id") Integer id) {
        return computeClient.delete(id);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public User find(@PathVariable("id") Integer id) {
        return computeClient.find(id);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET,
            headers = "Accept=application/json",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<User> list() {
        return computeClient.list();
    }
}
