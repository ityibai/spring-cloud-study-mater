package com.ityibai.spring.cloud.study5.study5eureka;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Study5EurekaApplicationTests {

	@Test
	public void contextLoads() {
	}

}
