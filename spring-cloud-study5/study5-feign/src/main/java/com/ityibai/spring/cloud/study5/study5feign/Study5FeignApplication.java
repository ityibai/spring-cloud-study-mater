package com.ityibai.spring.cloud.study5.study5feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class Study5FeignApplication {

	public static void main(String[] args) {
		SpringApplication.run(Study5FeignApplication.class, args);
	}
}
