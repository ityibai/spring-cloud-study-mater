/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 */
package com.ityibai.spring.cloud.study5.study5feign;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by panda on 2017/7/20.
 * Version : 1.0
 * Description : com.ityibai.spring.cloud.study5.study5feign.ComputeClientImpl
 */
@Component
public class ComputeClientImpl implements ComputeClient {
    private static final Logger log = LoggerFactory.getLogger(ComputeClientImpl.class);
    @Override
    public Integer add(@RequestParam Integer a, @RequestParam Integer b) {
        log.debug("出错啦~~~~~~");
        return -9999;
    }
}
