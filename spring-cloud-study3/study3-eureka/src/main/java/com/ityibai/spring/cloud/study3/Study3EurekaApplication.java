package com.ityibai.spring.cloud.study3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Study3EurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Study3EurekaApplication.class, args);
	}
}
